FROM node:14.7.0-alpine3.12

RUN apk add git

COPY package*.json /
RUN npm install
ENV PATH $PATH:/node_modules/.bin
RUN npm audit fix

RUN rm -rf /package*.json

RUN mkdir -p /code
WORKDIR /code

ENTRYPOINT semantic-release
